import sys
sys.path.append('../micropython_dummy_modules')
import random
from machine import Pin
from states import States

class LedStates(object):
    def __init__(self):
        self.leds = {
            'red': Pin('B0', Pin.OUT),
            'orange': Pin('E1', Pin.OUT),
            'green': Pin('B14', Pin.OUT)
        }
        return

    def all_of(self):
    #returns nothing; so gives 0 for all values in self.leds
        for pin in self.leds.values():
            pin.value(0)
        return

    def toggle_led(self, key):
        if self.leds[key].value():
            self.leds[key].value(0)
        else:
            self.leds[key].value(1)
        return

    def off(self, last_state, state):
        return last_state, state

    def red(self, last_state, state):
        return last_state, state

    def orange(self, last_state, state):
        return last_state, state

    def green(self, last_state, state):
        return last_state, state
